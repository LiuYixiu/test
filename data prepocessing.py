import numpy as np
import pandas as pd
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
import matplotlib.pyplot as plt


pd.set_option('display.max_columns', None)   #Show complete column
pd.set_option('display.max_rows', None)  #Show complete line

dataframe=pd.read_csv("C:/Users\Liu Yixiu\PycharmProjects\Keras-Semantic-Segmentation-master\data/task_data.csv",header=0,names=['class', 'sensor0', 'sensor1', 'sensor2', 'sensor3', 'sensor4', 'sensor5', 'sensor6', 'sensor7', 'sensor8', 'sensor9'])
print(dataframe.isnull().sum())
names=[ 'sensor0', 'sensor1', 'sensor2', 'sensor3', 'sensor4', 'sensor5', 'sensor6', 'sensor7', 'sensor8', 'sensor9']

for i in names:
    plt.hist(dataframe[dataframe['class'] == -1][i], 10, alpha=0.5,label='label 1')
    plt.hist(dataframe[dataframe['class'] == 1][i], 10, alpha=0.5, label='label -1')
    plt.legend(loc='upper right')
    plt.xlabel(i)
    plt.ylabel('Frequency')
    plt.title('Histogram of {}'.format(i))
    plt.show()

positiv=dataframe[dataframe['class']==1]
negativ=dataframe[dataframe['class']==-1]
print('1 result:{}'.format(positiv.describe()))
positiv.describe().to_excel('positiv.describe.xls',sheet_name='positiv')
print('-1 result:{}'.format(negativ.describe()))
negativ.describe().to_excel('negativ.describe.xls',sheet_name='negativ')
print('all result:{}'.format(dataframe.describe()))
dataframe.describe().to_excel('dataframe.describe.xls',sheet_name='all')
pearson=dataframe.iloc[:,1:].corr()
print('pearson result:{}'.format(pearson))
pearson.to_excel('pearson.xls',sheet_name='pearson')

X=dataframe.drop('class',axis=1)
Y=dataframe['class']
best=SelectKBest(score_func=f_classif,k=10)
fit=best.fit(X,Y)
kbest=[]
for i in range(10):
    kbest.append([fit.pvalues_[i],names[i]])
kbest.sort(key=lambda x:-x[0])
print("importance of the feature:{}".format(kbest))
